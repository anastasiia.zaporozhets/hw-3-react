import "../ProductCard/ProductCard.scss"
import PropTypes from "prop-types";
import Button from "../../Button/Button.jsx";
import ProductIconStar from "../ProductIconStar/ProductIconStar.jsx";
import ProductIconDelete from "../ProductIconDelete/ProductIconDelete.jsx";

function ProductCard({
                         item, onBuyClick, clickItem,
                         showStar = true, showDelete = false
                     }) {


    return (
        <div className="product-list__card">
            {showStar && <ProductIconStar clickItem={clickItem}
                                          item={item}

            />}
            {showDelete && <ProductIconDelete clickItem={clickItem} item={item}/>}

            <img src={item.imageUrl} alt="product" className="product-list__card__img"/>
            <div className="product-list__card__wrapper-text">
                <p className="product-list__card__wrapper-text__name">{item.name}</p>
                <p className="product-list__card__wrapper-text__color-text">Колір: {item.color}</p>
            </div>
            <div className="product-list__card__price-wrapper">
                <p className="product-list__card__price-wrapper__price">Ціна {item.price} грн</p>
            </div>
            <Button type="button" className="btn btn__main btn__main__first"
                    onClick={() => {
                        onBuyClick(item)
                    }}>
                КУПИТИ
            </Button>
        </div>
    );
}

ProductCard.propTypes = {
    item: PropTypes.shape({
        id: PropTypes.number.isRequired,
        imageUrl: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired,
        color: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired,
        article: PropTypes.string.isRequired,
    }).isRequired,
    onBuyClick: PropTypes.func,
    clickItem: PropTypes.func,
};

export default ProductCard