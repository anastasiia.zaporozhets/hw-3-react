import "./ModalFooter.scss"

function ModalFooter({children, className}) {

    return (
        <div className={className}>{children}</div>
    )
}

export default ModalFooter;