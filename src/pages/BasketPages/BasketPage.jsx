import ProductCard from "../../components/ProductsComponents/ProductCard/ProductCard.jsx";
import {useOutletContext} from "react-router-dom";
import ModalImage from "../../components/ModalComponents/ModalImage/ModalImage.jsx";
import "../BasketPages/BasketPage.scss"


function BasketPage({}) {

    const {basket, setBasket} = useOutletContext();
    const {modal, setModal} = useOutletContext();

    function openModalHandler(modalType, data = {}) {
        setModal({
            isOpen: true,
            modalType: modalType,
            data: data
        })
    }

    function closeModalHandler() {
        setModal({
            isOpen: false,
            modalType: null,
            data: {}
        })
    }


    function deleteBasketToLocalStorage(itemId) {
        const updatedBasket = basket.filter(item => item.id !== itemId);
        localStorage.setItem("basket", JSON.stringify(updatedBasket));
        setBasket(updatedBasket);
    }

    return (
        <section className="basket-page">
            <h1 className="basket-page__title">ДОДАНІ ТОВАРИ В КОШИК</h1>
            <div className="basket-page__wrapper-grid">
                {basket.map((item, index) => (
                    <ProductCard key={item.id}
                                 item={item}
                                 showStar={false}
                                 showDelete={true}
                                 clickItem={(item) => openModalHandler("image", item)}
                                 showButtonBuy={false}
                    />))}

                {modal.isOpen && modal.modalType === "image" && (
                    <ModalImage closeModal={closeModalHandler}
                                data={modal.data}
                                handlerToLocalStorage={() => deleteBasketToLocalStorage(modal.data.id)}
                                bodyQuestion={<p className="modal-body__text ">ВИДАЛИТИ ТОВАР З КОШИКА? </p>}

                    />)}
            </div>
        </section>
    )
}

export default BasketPage;