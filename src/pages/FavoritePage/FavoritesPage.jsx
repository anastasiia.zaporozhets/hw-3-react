import ProductCard from "../../components/ProductsComponents/ProductCard/ProductCard.jsx";
import {useOutletContext} from "react-router-dom";
import "../FavoritePage/FavoritePage.scss"

function FavoritesPage() {

    const {favorite, setFavorite} = useOutletContext();

    function deleteFavoritesToLocalStorage(itemId) {
        const updatedFavorite = favorite.filter(item => item.id !== itemId);
        setFavorite(updatedFavorite);
    }

    return (
        <section className="favorite-page">
            <h1 className="favorite-page__title">СПИСОК БАЖАНЬ</h1>
            <div className="favorite-page__wrapper-grid">
                {favorite.map((item, index) => (
                    <ProductCard key={item.id}
                                 item={item}
                                 favorite={favorite}
                                 setFavorite={setFavorite}
                                 clickItem={() => deleteFavoritesToLocalStorage(item.id)}/>
                ))}
            </div>
        </section>
    )
}


export default FavoritesPage;