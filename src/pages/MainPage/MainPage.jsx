import ModalImage from "../../components/ModalComponents/ModalImage/ModalImage.jsx";
import {useState, useEffect} from "react";
import Slider from "../../components/Slider/Slider.jsx";
import ProductsList from "../../components/ProductsComponents/ProductList/ProductList.jsx";
import {useOutletContext} from "react-router-dom";


function MainPage() {

    const {modal, setModal} = useOutletContext();

    function openModalHandler(modalType, data = {}) {
        setModal({
            isOpen: true,
            modalType: modalType,
            data: data
        })
    }

    function closeModalHandler() {
        setModal({
            isOpen: false,
            modalType: null,
            data: {}
        })
    }

    const [data, setData] = useState([]);

    useEffect(() => {
        fetch("products.json")
            .then(response => response.json())
            .then(data => {
                setData(data)
            })

    }, []);

    const {setBasket, basket, setFavorite, favorite} = useOutletContext();


    const addToFavorite = (item) => {
        if (!favorite.some(favItem => favItem.id === item.id)) {
            const updatedFavorite = [...favorite, item];
            setFavorite(updatedFavorite);
        }
    };
    //
    const addToBasket = () => {
        if (!basket.some(basketItem => basketItem.id === modal.data.id)) {
            const updatedBasket = [...basket, modal.data];
            setBasket(updatedBasket);
        } else {
            alert("Цей товар вже в корзині!");
        }
    };


    return (
        <>
            <Slider/>

            <ProductsList items={data}
                          onBuyClick={(itemData) => openModalHandler("image", itemData)}
                          clickItem={addToFavorite}

            />

            {modal.isOpen && modal.modalType === "image" && (
                <ModalImage closeModal={closeModalHandler}
                            data={modal.data}
                            handlerToLocalStorage={addToBasket}
                            bodyQuestion={<p className="modal-body__text ">ДОДАТИ ТОВАР ДО КОШИКА? </p>}
                />
            )}

        </>
    )
}

export default MainPage;