import Header from "./components/HeaderComponents/Header/Header.jsx";
import Footer from "./components/Footer/Footer.jsx";
import {Outlet, Link} from "react-router-dom";
import {useState} from "react";
import {useEffect} from "react";


function Layout() {

    const [basket, setBasket] = useState(() => {
        const basketSave = JSON.parse(localStorage.getItem('basket')) || []
        return basketSave;
    })

    const [favorite, setFavorite] = useState(() => {
        const favoriteSave = JSON.parse(localStorage.getItem("favorite")) || [];
        return favoriteSave;
    });

    const [modal, setModal] = useState({
        isOpen: false,
        modalType: null,
        data: {}
    });

    useEffect(() => {
        localStorage.setItem('favorite', JSON.stringify(favorite));
    }, [favorite]);

    useEffect(() => {
        localStorage.setItem('basket', JSON.stringify(basket));
    }, [basket]);



    return (
        <>
            <Header basket={basket.length} favorites={favorite.length}/>

            <Outlet context={{setBasket, setFavorite, favorite, basket, modal, setModal}}/>
            <Footer/>

        </>
    )
}

export default Layout;